#! /usr/bin/env python3


"""Determine what words can be formed from a phonenumber."""


import re
import unittest


class Decoder():
    keys = {
        2: ['a', 'b', 'c'],
        3: ['d', 'e', 'f'],
        4: ['g', 'h', 'i'],
        5: ['j', 'k', 'l'],
        6: ['m', 'n', 'o'],
        7: ['p', 'q', 'r', 's'],
        8: ['t', 'u', 'v'],
        9: ['w', 'x', 'y', 'z']}

    def checkWords(self, word, letters):
        i = 0
        for letter in letters:
            for l in letter:
                if len(word) > i and l == word[i]:
                    i += 1
                    yield l

    def numToLetters(self, phone):
        letters = (self.keys[int(num)] for num in str(phone))
        return letters

    def decode(self, phone, words):
        letters = list(self.numToLetters(phone))

        values = []
        w = ''
        for word in words:
            w += ''.join(self.checkWords(word, letters))
            if w == word:
                values.append(w)
            w = ''

        return values


def main():
    words = ['foo', 'bar', 'baz', 'foobar', 'emo', 'cap', 'car', 'cat']
    decoder = Decoder()
    d = decoder.decode(3662277, words)
    print(d)


if __name__ == '__main__':
    main()

    class Testing(unittest.TestCase):
        """Individual tests for methods."""

        decoder = Decoder()
        test_letters = [
            ['p', 'q', 'r', 's'],
            ['p', 'q', 'r', 's'],
            ['a', 'b', 'c'],
            ['m', 'n', 'o']]
        number = 7726

        def test_numToLetters(self):
            letters = self.decoder.numToLetters(self.number)
            self.assertEqual(list(letters), self.test_letters)

        def test_checkWords(self):
            test_word = ['s', 'p', 'a', 'm']
            word = self.decoder.checkWords(test_word, self.test_letters)
            self.assertEqual(list(word), test_word)

        def test_decode(self):
            word = self.decoder.decode(self.number, ['spam'])
            self.assertEqual(word, ['spam'])

    unittest.main()
