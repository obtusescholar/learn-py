#! /usr/bin/env python3


import unittest
from unittest.mock import patch


"""Draw a 'pyramid from top'

Layers: 3, example output:
CCCCC
CBBBC
CBABC
CBBBC
CCCCC
"""


def initiate(layers):
    figure = []
    outer_wall = layers * 2 - 1
    for _ in range(outer_wall):
        figure.append(list(layers for _ in range(outer_wall)))
    return figure


def toString(figure):
    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    string = ''

    for i, line in enumerate(figure):
        for cell in line:
            string += letters[cell - 1]
        string += '\n' if (i < len(figure) - 1) else ''

    return string


def draw(layers, figure=False):
    if figure is False:
        figure = initiate(layers)

    if layers <= 0:
        return toString(figure)

    wall = (layers - 1) * 2 - 1
    outer_wall = len(figure[0])
    start = int((outer_wall - wall) / 2)
    end = int(outer_wall - start)

    for a in range(start, end):
        if a == start or a == end-1:
            for b in range(start, end):
                figure[a][b] = layers - 1
        else:
            figure[a][start] = layers - 1
            figure[a][end - 1] = layers - 1
    
    return draw(layers-1, figure)


def main():
    layers = int(input('Layers: '))
    
    if 0 <= layers <= 26:
        print(draw(layers))
    else:
        raise ValueError('Give integer from 0 to 26')


if __name__ == '__main__':
    # main()

    class Testing(unittest.TestCase):
        """Tests for layers: 0,1,2,3,5,27,-1"""

        layers_0 = ''
        layers_1 = 'A'
        layers_2 = (
            'BBB\n'
            + 'BAB\n'
            + 'BBB')
        layers_3 = (
            'CCCCC\n'
            + 'CBBBC\n'
            + 'CBABC\n'
            + 'CBBBC\n'
            + 'CCCCC')
        layers_5 = (
            'EEEEEEEEE\n'
            + 'EDDDDDDDE\n'
            + 'EDCCCCCDE\n'
            + 'EDCBBBCDE\n'
            + 'EDCBABCDE\n'
            + 'EDCBBBCDE\n'
            + 'EDCCCCCDE\n'
            + 'EDDDDDDDE\n'
            + 'EEEEEEEEE')

        def test_toString_0(self):
            picture = draw(0)
            self.assertEqual(picture, self.layers_0)

        def test_toString_1(self):
            picture = draw(1)
            self.assertEqual(picture, self.layers_1)

        def test_toString_2(self):
            picture = draw(2)
            self.assertEqual(picture, self.layers_2)

        def test_toString_3(self):
            picture = draw(3)
            self.assertEqual(picture, self.layers_3)

        def test_toString_5(self):
            picture = draw(5)
            self.assertEqual(picture, self.layers_5)

        @patch('builtins.input', return_value=27)
        def test_toString_27(self, i):
            with self.assertRaises(ValueError):
                main()

        @patch('builtins.input', return_value=-1)
        def test_toString_neg1(self, i):
            with self.assertRaises(ValueError):
                main()


    unittest.main()
