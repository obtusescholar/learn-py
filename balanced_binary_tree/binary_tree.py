#! /usr/bin/python3


"""Learning how to use Balanced Binary Tree"""


import unittest


class Branch():
    """Binary Tree"""

    def __init__(self,
                 value: int,
                 left: 'Branch' = None,
                 right: 'Branch' = None):
        """Take root value and assume None values for branches"""

        self.value = value
        self.left = left
        self.right = right

    def __repr__(self):
        """Outputs Tree values that can be used comparing trees"""

        return f'Branch({self.value} left:{self.left} right:{self.right})'


def fill_tree_programmatically(values: list, start: int, end: int):
    """Fill Balanced Binary Tree with given values"""

    # Start and end points are overlapping, branch has reached its endpoint
    if start > end:
        return None

    # Get the midpoint and make a new branch with it
    mid = (start + end) // 2
    root = Branch(values[mid])

    # Make left branch with smaller values than midpoint
    root.left = fill_tree_programmatically(values, start, mid - 1)

    # Make right branch with bigger values than midpoint
    root.right = fill_tree_programmatically(values, mid + 1, end)

    # Return whole tree after the recursion is finished
    return root


def fill_tree_manually():
    """Manually balanced binary tree as a point of copmarison"""

    # used values: range(1, 10)
    # Trunk
    branch = Branch(5)

    # Left Branch
    # If value is smaller than branch
    branch.left = Branch(2)
    branch.left.left = Branch(1)
    branch.left.right = Branch(3)
    branch.left.right.right = Branch(4)

    # Right Branch
    # If value is bigger than branch
    branch.right = Branch(7)
    branch.right.left = Branch(6)
    branch.right.right = Branch(8)
    branch.right.right.right = Branch(9)

    return branch


def find_value_from_balanced_tree(tree: Branch, value: int):
    """Find and return value from tree, return False if not found"""

    # If branch is None -> value is not in tree
    if tree is None:
        return False

    # If value is found -> return it
    elif value == tree.value:
        return tree.value

    # If value is smaller -> check left branch
    elif value < tree.value:
        return find_value_from_balanced_tree(tree.left, value)

    # If value is bigger -> check right branch
    elif value > tree.value:
        return find_value_from_balanced_tree(tree.right, value)


def sum_tree_values(tree: Branch):
    """Count all tree values together"""

    # Return 0 if branch does not exist
    if tree is None:
        return 0

    # Count tree value and add in left and right values recursively
    value = tree.value
    value += sum_tree_values(tree.left)
    value += sum_tree_values(tree.right)

    # Return the whole value
    return value


def find_biggest_value(tree: Branch):
    """Returns the biggest value in the tree"""

    biggest = tree.value

    if tree.right is not None:
        biggest = find_biggest_value(tree.right)

    return biggest


if __name__ == '__main__':
    class Test_Tree(unittest.TestCase):
        """Test that BBT is correctly created and functions work"""

        def test_compare_manual_and_program_trees(self):
            """Test programmatically making Balanced Binary Tree

            Check if repr output equals to manually balanced tree
            """

            manual_tree = fill_tree_manually()

            values = list(range(1, 10))
            start = 0
            end = len(values) - 1
            program_tree = fill_tree_programmatically(values, start, end)

            self.assertEqual(repr(manual_tree), repr(program_tree))

        def test_find_from_tree_97(self):
            """Test that tree returns the same given 97 value"""

            values = list(range(10, 100))
            tree = fill_tree_programmatically(values, 0, len(values) - 1)

            found = find_value_from_balanced_tree(tree, 97)

            self.assertEqual(found, 97)

        def test_find_from_tree_Not_Found(self):
            """Test value is too big, function returns False"""

            values = list(range(10, 100))
            tree = fill_tree_programmatically(values, 0, len(values) - 1)

            found = find_value_from_balanced_tree(tree, 200)

            self.assertEqual(found, False)

        def test_sum_tree_values(self):
            """Check that tree values counted equals sum()"""

            values = list(range(10, 100))
            tree = fill_tree_programmatically(values, 0, len(values) - 1)

            tree_summed = sum_tree_values(tree)

            self.assertEqual(tree_summed, sum(values))

        def test_find_biggest_value(self):
            """Check that find_biggest_value equals max()"""

            values = list(range(10, 100))
            tree = fill_tree_programmatically(values, 0, len(values) - 1)

            biggest_value = find_biggest_value(tree)

            self.assertEqual(biggest_value, max(values))


    unittest.main()
