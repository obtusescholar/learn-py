#! /usr/bin/env python3


from functools import partial


def loadFile(path):
    with open(path) as f:
        rfile = f.read()
    return rfile


def wordList():
    return loadFile('wordlist.txt').split()


def askText():
    if True:
        txt = input('Write text: ').split()
    else:
        txt = 'This is acually a good and usefull program'.split()

    return txt


def markWrong(wordlist, word):
    if word.casefold() in wordlist:
        return word
    else:
        return '*' + word + '*'


def main():
    markFromWordlist = partial(markWrong, wordList())
    print(' '.join(map(markFromWordlist, askText())))


if __name__ == '__main__':
    main()
