#! /usr/bin/env python3


import math
import unittest
from datetime import datetime

import pygame


"""Analog clock rendered with pygame"""


# Constants
SCREENSIZE = (640, 480)
CENTER = (SCREENSIZE[0] / 2, SCREENSIZE[1] / 2)
BGCOLOR = (51, 59, 65)


class Hand():
    __hand = {
        'second': {
            'w': 2,
            'h': 180,
            'color': (229, 239, 246),
            'revolution': 60},
        'minute': {
            'w': 4,
            'h': 170,
            'color': (229, 239, 246),
            'revolution': 60},
        'hour': {
            'w': 8,
            'h': 100,
            'color': (229, 239, 246),
            'revolution': 12}}

    def __init__(self,
                 time_type: str,
                 center: tuple = CENTER):
        """time_type: second, minute, hour"""

        self.__data = self.__hand[time_type]
        self.__time_type = time_type
        self.__center = center

    @classmethod
    def time(cls):
        """Return current time as string"""

        return datetime.now().strftime('%H:%M:%S')

    def __hand_time(self):
        """Return time value of the current clock hand"""

        time_list = Hand.time().split(':')
        if self.__time_type == 'second':
            time = time_list[2]
        elif self.__time_type == 'minute':
            time = time_list[1]
        elif self.__time_type == 'hour':
            time = time_list[0]

        return int(time)

    def unit_circle(self, time: int = None):
        """Return unit circle factor"""

        if time is None:
            time = self.__hand_time()
        angle = (2 * math.pi * time) / self.__data['revolution']

        return (round(math.sin(angle), 3), round(math.cos(angle), 3))

    def end_points(self, time: int = None):
        """Unit circle factor changed into coordinates

        Unit circle Y value is multiplied with -1 because the screen
        goes starts from top and ends at bottom."""

        unit_x, unit_y = self.unit_circle(time)

        x = self.__center[0] + self.__data['h'] * unit_x
        y = self.__center[1] + self.__data['h'] * (-unit_y)

        return (x, y)

    def color(self):
        """Return hand color"""

        return self.__data['color']

    def start_points(self):
        """Return hand starting points"""

        return self.__center

    def width(self):
        """Return hand width"""

        return self.__data['w']

    def height(self):
        """Return hand height"""

        return self.__data['h']


def draw_face(screen: pygame.Surface,
         size: int,
         color: tuple = (229, 239, 246),
         location: tuple = CENTER):
    """Draw clock face"""

    pygame.draw.circle(screen, color, location, size)


def draw_hand(screen: pygame.Surface, hand: Hand):
    """Draw clock hand"""

    pygame.draw.line(
        screen,
        hand.color(),
        hand.start_points(),
        hand.end_points(),
        hand.width())


def main():
    """Pygame main function"""

    pygame.init()
    screen = pygame.display.set_mode(SCREENSIZE)
    pygame_tick = pygame.time.Clock()

    second = Hand('second')
    minute = Hand('minute')
    hour = Hand('hour')

    while True:
        pygame.display.set_caption(Hand.time())

        screen.fill(BGCOLOR)

        draw_face(screen, 200)
        draw_face(screen, 195, BGCOLOR)
        draw_face(screen, 10)

        draw_hand(screen, second)
        draw_hand(screen, minute)
        draw_hand(screen, hour)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()

        pygame.display.flip()
        pygame_tick.tick(60)


if __name__ == '__main__':
    class Test_Hand(unittest.TestCase):
        second = Hand('second')
        minute = Hand('minute')
        hour = Hand('hour')

        def test_unit_circle_0_seconds(self):
            """test seconds at 12 o'clock"""

            x, y = self.second.unit_circle(0)
            self.assertEqual(round(x, 1), 0)
            self.assertEqual(round(y, 1), 1)

        def test_unit_circle_9_hours(self):
            """test hours at 270 degree angle"""

            x, y = self.hour.unit_circle(9)
            self.assertEqual(round(x, 1), -1)
            self.assertEqual(round(y, 1), 0)

        def test_end_points_5_minutes(self):
            """test minutes with 30 degree unit circle values

            cos: 1/2
            sin: sqrt(3) / 2

            Y value is multiplied with -1 because the screen goes from
            top to bottom.
            """

            x_start, y_start = self.minute.start_points()

            x = round(x_start + 0.5 * self.minute.height(), 2)
            y = round(y_start - math.sqrt(3) / 2 * self.minute.height(), 2)

            x_end, y_end = self.minute.end_points(5)

            self.assertEqual(x_end, x)
            self.assertEqual(y_end, y)


    if True:
        main()
    else:
        unittest.main()
