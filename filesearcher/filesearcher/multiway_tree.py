#! /usr/bin/env python3


from dataclasses import dataclass
from pathlib import Path
from typing import List, Optional

from fuzzywuzzy import fuzz


@dataclass
class FileSystemNode:
    def __init__(
        self, name: str = "", path: str = "", is_directory: bool = False
    ) -> None:
        self.name: str = name
        self.path: str = path
        self.is_directory: bool = is_directory
        self.children: List[FileSystemNode] = []

    def add_child(self, child_node: "FileSystemNode") -> None:
        self.children.append(child_node)

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(name='{self.name}', path='{self.path}', is_dictionary={self.is_directory})"


def build_filesystem_tree(path: Path) -> FileSystemNode:
    root: FileSystemNode = FileSystemNode(
        name=path.name, path=str(path), is_directory=True
    )

    for item in path.iterdir():
        is_directory: bool = item.is_dir()
        node: FileSystemNode = FileSystemNode(
            name=item.name, path=str(item), is_directory=is_directory
        )
        root.add_child(node)
        if is_directory:
            node.children = build_filesystem_tree(item).children

    return root


def dfs_search(node: FileSystemNode, name: str) -> Optional[FileSystemNode]:
    if node.name == name:
        return node

    for child in node.children:
        result = dfs_search(child, name)
        if result:
            return result

    return None


def fuzzy_search(
    node: FileSystemNode, query: str, threshhold: int = 80
) -> List[FileSystemNode]:
    matches: List[FileSystemNode] = []

    # check if the name of the current node is a fuzzy match with the search string
    score: int = fuzz.partial_ratio(node.name, query)
    if score >= threshhold:
        matches.append(node)

    # recursively search the children for matches
    for child in node.children:
        child_matches: List[FileSystemNode] = fuzzy_search(child, query)
        matches.extend(child_matches)

    return matches


if __name__ == "__main__":
    # Ask the user for the root directory of the file system hierarchy
    root_path_str: str = input("Enter the root path of the file system hierarchy: ")
    root_path: Path = Path(root_path_str).expanduser().resolve()

    # Build the file system tree
    root: FileSystemNode = build_filesystem_tree(root_path)

    # Search for a file or directory by name
    search_name: str = input("Enter the name of a file or directory to search for: ")
    results: List[FileSystemNode] = fuzzy_search(root, search_name)

    if results:
        print(f"Found {len(results)} matches for '{search_name}':")
        for result in results:
            if result.is_directory:
                print(f"  {result.name}/")
            else:
                print(f"  {result.name}")
            # print(repr(result))
    else:
        print(f"No matches found for '{search_name}'")
